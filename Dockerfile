FROM python:3-alpine
RUN mkdir /app
WORKDIR /app
COPY requirements.txt /requirements.txt
COPY src/ /app
RUN pip install -r /requirements.txt
CMD ["gunicorn", "-w 2", "-b", "0.0.0.0:8080","-t 120", "run:app", "--access-logfile","-"]
