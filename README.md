# lichess-info

Lichess-info provides :

* team members and status
* team leaderboard by game
* team online and playing notifications (slack hook)
* slack command `/li` but require commercial slack version (deprecated)

Demo: https://li.valvin.fr/teams

## Configuration

### Config file

```yaml
---
lichess:
  # token to get private information such as private team membership
  api-token: '****'
default:
  # team used by default
  team: think-echecs
  # filtered game on podium
  game-filter: rapid,blitz
slack:
  # hook used for slack notifications
  hook-url: https://hooks.slack.com/services/****/****/*****
  # channel associated to the hook
  channel: my_chess_channel
redis:
  # redis server hostname
  server: redis-db
notifications:
  # to switch on/off notifications
  team-member-online: false
  team-member-offline: true
  team-member-game-starting: true
  team-member-game-finishing: true

```

### Environment variable
| Environment variable | default value | sample value | comment |
| --- | --- | --- | --- |
| `LICHESS_CONFIG_FILE` | | `<working dir>/config.yaml` |(optional) allowes to specify a specific config file |


## Launch from code

```bash
# optional (but recommended): create and activate a virtual env
python -m venv venv
. venv/bin/activate
pip install -r requirements.txt
# adjust you config.yaml file
cp config.sample.yaml config.yaml
vim config.yaml
cd src
python run.py
```

## Launch container:

An image is available here `registry.gitlab.com/valvin/lichess-info:latest`

```bash
# for testing purpose
docker run --rm -it -p 8080:8080 -v $(pwd)/config.yaml:/app/config.yaml registry.gitlab.com/valvin/lichess-info:latest 
# or
podman run --privileged --rm -it -p 8080:8080 -v $(pwd)/config.yaml:/app/config.yaml registry.gitlab.com/valvin/lichess-info:latest 
```