from lichess_info.lib.lichess import LichessGame
from tests.utils import load_sample

def test_game_finished():
    data = load_sample('game_finished_lost')
    player1 = load_sample('user_info_valvin1')
    player2 = load_sample('user_info_abs_3975')
    game = LichessGame(data,player1,player2)
    assert game.id == 'hPf1GABv'
    assert game.finished == True
    assert game.white_player.id == 'abs_3975'
    assert game.black_player.id == 'valvin1'
    assert game.has_won('valvin1') == False
    assert game.white_player.updated_elo == 1252
    assert game.black_player.updated_elo == 1241
    assert game.game_url('valvin1') == 'https://lichess.org/hPf1GABv/black'

def test_game_playing():
    data = load_sample('game_playing')
    player1 = load_sample('user_info_destroyernumber1')
    player2 = load_sample('user_info_flyingbishop97')
    game = LichessGame(data,player1,player2)
    assert game.id == '7xEZH5bB'
    assert game.finished == False
    assert game.white_player.id == 'flyingbishop97'
    assert game.black_player.id == 'destroyernumber1'
    assert game.white_player.elo == game.white_player.updated_elo
    assert game.black_player.elo == game.black_player.updated_elo
    assert game.game_url() == 'https://lichess.org/7xEZH5bB'
    
