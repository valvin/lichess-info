import json,os

def load_sample(name:str):
    data = None
    with open(f"{os.path.dirname(__file__)}/data/{name}.json",'r') as f:
        dataJSON = f.read()
        data = json.loads(dataJSON)
    return data