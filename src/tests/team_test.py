from tests.utils import load_sample
from lichess_info.lib.lichess import LichessTeam


def test_team_init():
    team = LichessTeam('think-echecs')
    assert team.team_name == 'think-echecs'
    assert len(team.members) == 0

def test_team_add_members():
    raw_data = load_sample('team_members_think-echecs')
    team = LichessTeam('think-echecs')
    team.add_members(raw_data)
    assert len(team.members) == 4
    assert 'valvin1' in team.get_members_ids()

def test_team_add_member_twice():
    raw_data = load_sample('team_members_think-echecs')
    team = LichessTeam('think-echecs')
    team.add_members(raw_data)
    team.add_members(raw_data)
    assert len(team.members) == 4
