from re import L
from lichess_info.lib.cache.redis import LichessInfoCache
from typing import List
from time import sleep
from datetime import datetime
from lichess_info.lib.lichess import LichessApi, LichessTeam, LichessUser
from lichess_info.lib.cache import LichessInfoCache
from lichess_info.lib.lichess.lichess_game import LichessGame
from lichess_info.lib.slack import SlackHook,SlackColor
from lichess_info import Config
import logging

logger = logging.getLogger(__name__)

class LichessInfoController:
    config: Config
    lichess_api: LichessApi
    cache: LichessInfoCache

    def __init__(self) -> None:
        self.config = Config()
        self.cache = LichessInfoCache(self.config.redis_server)
        self.lichess_api = LichessApi(self.config.lichess_api_token)

    def get_default_team(self) -> LichessTeam:
        if self.config.default_team != '':
            return self.lichess_api.get_team(self.config.default_team)
        else:
            return None
    
    def __refresh_users_cache(self, users: list[LichessUser]) -> None:
        for u in users:
            self.__refresh_user_cache(u)

    def __refresh_user_cache(self, user: LichessUser) -> None:
        self.cache.add_cache('user', user.id, 
                                {
                                    "online": str(user.online),
                                    "playing": str(user.playing),
                                    "game_id": user.current_game.id if user.current_game else '' ,
                                    "last_update": str(datetime.now())
                                })
    
    def get_slack_hook(self) -> SlackHook:
        if self.config.slack_hook_url != '':
            return SlackHook(self.config.slack_hook_url, self.config.slack_channel)
        else:
            None


    def __analyze_and_notify_team_members_cache(self, team: LichessTeam) -> None:

        slack_hook = self.get_slack_hook()

        for m in team.members:
            refresh_user_cache = True
            last_member_info = self.cache.get_cache('user', m.id)
            if m.check_goes_online(last_member_info):
                if self.config.notify_team_member_online:
                    logger.info(f"{m.id} is now online")
                    if slack_hook:
                        slack_hook.send_message("",f"{m.id} is now online", color=SlackColor.GREEN)
            if m.check_ends_game(last_member_info):
                if self.config.notify_team_member_game_finishing:
                    game = self.lichess_api.get_game(last_member_info['game_id'])
                    #FIXME: dirty fix to avoid missing end games
                    if not game.finished:
                        max_retry_attempt = 5
                        delay_between_attempt_seconds = 0.5
                        attempt = 1
                        while not game.finished and attempt < max_retry_attempt:
                            logger.warning(f"{game.id} is not considered finished. waiting {delay_between_attempt_seconds * 1000}ms and trying to refresh {attempt}/{max_retry_attempt}")
                            sleep(delay_between_attempt_seconds)
                            game = self.lichess_api.get_game(last_member_info['game_id'])
                            attempt += 1
                    if game.finished:
                        winner = game.get_winner()
                        color = None
                        title = ''
                        if winner:
                            if winner.id == m.id:
                                logger.info(f"{m.id} finished and won game {last_member_info['game_id']}")
                                color = SlackColor.GREEN
                                title = "Bravo !"
                                if winner.elo_diff:
                                    message = f"{m.id} won the game and +{winner.elo_diff} elo points ({game.speed})"
                                else:
                                    message = f"{m.id} won the game ({game.speed})"
                            else:
                                logger.info(f"{m.id} finished and lost game {last_member_info['game_id']}")
                                looser = game.get_looser()
                                color = SlackColor.RED
                                title = "Oups !"
                                if looser.elo_diff:
                                    message = f"{m.id} lost the game and {looser.elo_diff} elo points ({game.speed})"
                                else:
                                    message = f"{m.id} lost the game ({game.speed})"
                        else:
                            logger.info(f"{m.id} finished and drawn game {last_member_info['game_id']} ({game.speed})")
                            color = SlackColor.GREY
                            title = "Oh !?"
                            if game.status != 'aborted':
                                message = f"{m.id} has drawn its game ({game.speed})"
                            else:
                                message = f"{m.id} game aborted ({game.speed})"
                        if slack_hook: 
                            slack_hook.send_message(title, message, color=color,
                                fields_kv=self.get_game_info(game, m.id),
                                title_link=game.game_url(m.id))
                    else:
                        refresh_user_cache = False
                        logger.warning(f"Game {game.id} is not yet finished")

            if m.check_starts_new_game(last_member_info):
                if self.config.notify_team_member_game_starting :
                    logger.info(f"{m.id} is playing {m.current_game.id}")
                    if slack_hook:
                       slack_hook.send_message("", f"{m.id} is now playing a {m.current_game.speed} game", 
                        color=SlackColor.BLUE,
                        fields_kv=self.get_game_info(m.current_game,m.id),
                        title_link=m.tv_url
                        )
            if m.check_goes_offline(last_member_info):
                if self.config.notify_team_member_offline :
                    logger.info(f"{m.id} is now offline")
                    if slack_hook:
                        slack_hook.send_message("",f"{m.id} is now offline", color=SlackColor.RED)
            
            if refresh_user_cache:
                self.__refresh_user_cache(m)

    def get_game_info(self, game: LichessGame, user_id: str):
        game_info = {}
        if game.finished:
            game_info = {
                'white_player': f"{game.white_player.id} - {game.white_player.elo} -> {game.white_player.updated_elo}",
                'black_player': f"{game.black_player.id} - {game.black_player.elo} -> {game.black_player.updated_elo}",
                'status': game.status
            }
            if game.opening:
                game_info['opening'] = game.opening
        else:
            game_info = {
                'white_player': f"{game.white_player.id} - {game.white_player.elo}",
                'black_player': f"{game.black_player.id} - {game.black_player.elo}",
            }
        return game_info


    def refresh_default_team_cache(self):
        team = self.get_default_team()
        self.__analyze_and_notify_team_members_cache(team)