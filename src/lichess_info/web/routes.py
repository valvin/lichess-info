from email.policy import default
from flask import render_template, request, Response
import json
from lichess_info.lib.lichess import LichessApi
from lichess_info.lib.slack import SlackHook, SlackColor
from lichess_info import Config, app
from lichess_info.web.controller import LichessInfoController
from redis import Redis,ConnectionError

instance = None
controller: LichessInfoController = None

# FIXME: extract instance into a dedicated class
def init():
    global controller
    if not controller:
        controller = LichessInfoController()
        app.logger.debug("Creating a new instance")
        if controller.cache.up():
            controller.refresh_default_team_cache()

@app.route('/about')
def index():
    return render_template('about.html')

@app.route('/teams/<team_id>')
def team_detail(team_id):
    global controller
    init()
    team = controller.lichess_api.get_team(team_id)
    podiums = team.get_podiums(filter=controller.config.default_game_filter.split(','))
    return render_template('team.html', team=team, podiums=podiums)

@app.route('/teams')
@app.route('/')
def team_detail_default():
    global controller
    init()
    team = controller.get_default_team()
    if team:
        podiums = team.get_podiums(filter=controller.config.default_game_filter.split(','))
        return render_template('team.html', team=team, podiums=podiums)
    else:
        app.logger.error('no default team')
        return 'no default team, please setup a default team in config file', 404

@app.route('/tasks/refresh',methods=["POST"])
def refresh_lichess_info():
    global controller
    init()
    controller.refresh_default_team_cache()
    return 'done', 200

#@app.route('/test')
#def test():
#    global instance
#    init()
#    g = instance['li'].get_last_game_synthesis('cowreth')
#    app.logger.debug(g)
#    slack_hook = SlackHook(instance['conf'].slack_hook_url, instance['conf'].slack_channel) if instance['conf'].slack_hook_url != '' else None
#    slack_hook.send_message('my title', 'a wonderful message', 'Hey do that!',color=SlackColor.YELLOW,fields_kv=g)
#    return 'yes', 200

#@app.route('/user/<user_id>')
#def get_user(user_id):
#    global instance
#    init()
#    result = instance['li'].get_user(user_id)
#    app.logger.debug(result)
#    return result[0], 200
#
#@app.route('/game/<game_id>')
#def get_game(game_id):
#    global instance
#    init()
#    result = instance['li'].get_game(game_id)
#    app.logger.debug(result)
#    return result, 200
#@app.route('/slack/command/<command>',methods=["POST"])
#def slack_command(command):
#    if command == 'li':
#        li_available_commands = ['board', 'who']
#        default_command = li_available_commands[0]
#        command_text = request.values.get('text')
#        args = []
#        if command_text:
#            args = command_text.split(' ')
#        command = ''
#        if len(args) > 0:
#            if args[0] in li_available_commands:
#                command = args[0]
#            else:
#                return f"invalid command: `/li {'|'.join(li_available_commands)} ` (only one command at the moment)",200
#        else:
#            command = default_command
#
#        if Config.LICHESS_DEFAULT_TEAM != '':
#            lichess_info = LichessInfo(Config.LICHESS_API_TOKEN)
#            team = lichess_info.get_team(Config.LICHESS_DEFAULT_TEAM)
#            podiums = team.get_podiums()
#            slack_message = SlackMessage()
#            if command == 'board':
#                slack_message.create_lichess_podium(podiums)
#                slack_message.in_channel = False
#            elif command == 'who':
#                slack_message.create_team(team)
#                slack_message.in_channel = False
#
#            return Response(slack_message.get_payload(), mimetype="application/json", status=200)
#        else:
#            return 'no default team setup', 200
#    else:
#        return 'malformed', 400
#