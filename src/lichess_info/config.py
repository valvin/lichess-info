import os,yaml, logging

class Config:

    def __init__(self) -> None:
        config_file = Config.get_env('LICHESS_CONFIG_FILE',f"{os.getcwd()}/config.yaml")
        with open(config_file,'r') as file:
            self.conf = yaml.safe_load(file)
        
        self.default_team = self.get_value('default', 'team')
        self.default_game_filter = self.get_value('default', 'game-filter','rapid,blitz')
        self.redis_server = self.get_value('redis','server', 'localhost')
        self.lichess_api_token = self.get_value('lichess', 'api-token')
        self.slack_hook_url = self.get_value('slack','hook-url')
        self.slack_channel = self.get_value('slack', 'channel')
        self.notify_team_member_online = self.get_value('notifications', 'team-member-online', False)
        self.notify_team_member_offline = self.get_value('notifications', 'team-member-offline', False)
        self.notify_team_member_game_starting = self.get_value('notifications', 'team-member-game-starting', False)
        self.notify_team_member_game_finishing = self.get_value('notifications', 'team-member-game-finishing', False)
        self.server_host = self.get_value('server', 'host', '127.0.0.1')
        self.server_port = self.get_value('server', 'port', 5000)
        self.server_debug = self.get_value('server', 'debug', False)
        _log_level = self.get_value('log', 'level', 'INFO')
        self.log_level = logging.INFO
        if _log_level == 'WARN':
            self.log_level = logging.WARN
        elif _log_level == 'ERROR':
            self.log_level = logging.ERROR
        elif _log_level == 'CRITICAL': 
            self.log_level = logging.CRITICAL
        elif _log_level == 'DEBUG': 
            self.log_level = logging.DEBUG

    def get_value(self,category: str, key: str, default:str='') -> str:
        if category in self.conf and key in self.conf[category]:
            return self.conf[category][key]
        else:
            return default
    
    @staticmethod
    def get_env(name, default=''):
        return os.environ[name] if name in os.environ else default