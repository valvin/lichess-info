from redis import Redis,ConnectionError
import logging
logger = logging.getLogger(__name__)

class LichessInfoCache:

    server: Redis

    def __init__(self, host) -> None:
        self.server = Redis(host=host, decode_responses=True)
    
    def up(self) -> bool:
        try:
            self.server.ping()
            return True
        except:
            return False
    
    def add_cache(self, section: str, id: str, value: dict) -> None:
        #logger.debug(f"add cache: {section}:{id} => {value}")
        self.server.hmset(f"{section}:{id}", value)
    
    def get_cache(self, section:str, id:str) -> dict:
        values = self.server.hgetall(f"{section}:{id}") 
        #logger.debug(f"get cache {section}:{id} => {values}")
        return values 