import json, requests,logging

logger = logging.getLogger(__name__)
class SlackMessage:
    def __init__(self) -> None:
        self.text = ''
        self.in_channel = False
    
    def create_lichess_podium(self, podiums ):
        response = ''
        for g,p in podiums.items():
            if len(p.items()) > 0:
                response += f"*{g}*\n\n"
                first = True
                for user, result in p.items():
                    if first:
                        response += ':crown: '
                        first = False
                    if result.progression != 0:
                        response += f"{user} - {result.rating} ({'+' if result.progression > 0 else ''}{result.progression}) - {result.plays}\n"
                    else:
                        response += f"{user} - {result.rating} - {result.plays}\n"
                response += '\n'
        self.text = response

    def create_team(self, team ):
        response = f"*{team.team_name}*\n\n"
        for member in team.get_members_sorted_by_status():
            response += ":large_green_circle: " if member.online else ":zzz: "
            response += member.id
            response += f" :tv: {member.profile_url}/tv" if member.playing else ''
            response += '\n'
        self.text = response
    
    def get_payload(self):
        payload = {
            'response_type': 'in_channel' if self.in_channel else 'ephemeral',
            'text': self.text
        }
        
        return json.dumps(payload)

class SlackColor:
    GREEN = '#32a852'
    GREY = '#6f6f6f'
    BLUE = '#004f6f'
    RED = '#6f0f10'
    YELLOW = '#bd9707'
class SlackHook:
    
    hook_url: str
    channel: str
    def __init__(self, hook_url: str, channel: str):
        self.hook_url = hook_url
        self.channel = channel
    
    def send_message(self,title: str, message: str, action_message: str=None,color: str=SlackColor.GREEN, fields_kv: dict=None, title_link: str=None) -> int:
        data = {
            'text': title,
            'icon_emoji': ':chess_pawn:',
            'channel': self.channel,
            'username': 'Lichess info',
            'attachments': [
                {
                    'color': color,
                    'title': message,
                }
            ]
        }

        if title_link:
            data['attachments'][0]['title_link'] = title_link

        if action_message:
            data['attachments'][0]['markdwn_in'] = ['text']
            data['attachments'][0]['text'] = action_message

        if fields_kv:
            fields = []
            for k,v in fields_kv.items():
                fields.append({
                    'title': str(k).replace('_', ' '),
                    'value': str(v),
                    'short': True
                })

            data['attachments'][0]['fields'] = fields
            logger.debug(json.dumps(data, default=str, indent=4))

        r = requests.post(self.hook_url,json=data)
        if r.status_code != 200:
            logger.error(f"Slack message not sent: {r.status_code} - {r.text}")
        return r.status_code