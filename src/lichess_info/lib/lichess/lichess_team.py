from datetime import datetime
import logging
from typing import List
from lichess_info.lib.lichess.lichess_user import LichessUser
logger = logging.getLogger(__name__)

class LichessTeam:
    team_name: str
    members: list[LichessUser]

    def __init__(self,team_name:str) -> None:
        self.team_name = team_name
        self.members = []
    
    def add_member(self, member: LichessUser) -> None:
        for m in self.members:
            if m.id == member.id:
                logger.warning(f"trying to add {member.id} in {self.team_name} twice")
                return
        self.members.append(member)
    
    def add_members(self, raw_team_members: dict) -> None:
        for member in raw_team_members:
            l_user = LichessUser(member)
            self.add_member(l_user)
    
    def get_members_ids(self) -> list[str]:
        members_ids = []
        for member in self.members:
            members_ids.append(member.id)
        return members_ids


    
    def __str__(self) -> str:
        return f"{self.team_name} with {len(self.members)} members"
        
    def get_games_from_members(self) -> dict:
        games = []
        for m in self.members:
            if m.perfs:
                for game in m.perfs.keys():
                    if game not in games:
                        games.append(game)
        return games
    
    def get_members_sorted_by_status(self) -> list[LichessUser]:
        def sort_by_status(m):
            value = 0
            if m.online:
                value +=1
            if m.playing:
                value +=1
            return value
        return sorted(self.members,reverse=True, key=sort_by_status)

    def get_podiums(self, filter:list = []):
        games = self.get_games_from_members()
        podiums = {}
        for g in games:
            if len(filter) == 0 or g in filter:
                for m in self.members:
                    if m.perfs and g in m.perfs:
                        if m.perfs[g].rating_confirmed:
                            if not g in podiums:
                                podiums[g] = {}
                            podiums[g][m.id] = m.perfs[g] 
                if g in podiums:
                    podiums[g] = dict(sorted(podiums[g].items(),key= lambda x:-x[1].rating))
        return podiums