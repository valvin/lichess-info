from __future__ import annotations

from datetime import datetime
import json,logging
from typing import List, TYPE_CHECKING

if TYPE_CHECKING:
    from lichess_info.lib.lichess import LichessGame


logger = logging.getLogger(__name__)

class LichessUser:

    __raw_data: dict 
    id: str
    profile_url: str
    online: bool = False
    playing: bool = False
    current_game: LichessGame = None
    last_seen: datetime = None
    perfs: dict = None

    def __init__(self, raw_data) -> None:
        
        self.__raw_data = raw_data
        self.id = self.__raw_data['id']
        self.profile_url = self.__compute_profile_url()
        self.tv_url = f"https://lichess.org/@/{self.id}/tv"
        self.last_seen = self.__raw_data['seenAt'] if 'seenAt' in self.__raw_data else None
        self.perfs = self.compute_perfs()

    def __compute_profile_url(self) -> str:
        if 'profile' in self.__raw_data and 'url' in self.__raw_data['profile'] :
            return self.__raw_data['profile']['url'] 
        elif 'url' in self.__raw_data:
            return self.__raw_data['url']

    def add_realtime_data(self, rt_data) -> None:
        self.online = rt_data['online'] if 'online' in rt_data else False
        self.playing = rt_data['playing'] if 'playing' in rt_data else False
    
    def add_current_game(self, game : LichessGame) -> None:
        self.current_game = game

    def compute_perfs(self) -> dict:
        perfs = {}
        if 'perfs' in self.__raw_data:
            for game in self.__raw_data['perfs']:
                g_perf = LichessGamePerf(game, self.__raw_data['perfs'].get(game))
                perfs[game] = g_perf
            self.perfs = perfs
            return perfs
        else:
            return None

    def check_goes_online(self, last_member_info: dict) -> bool:
        if 'online' in last_member_info and self.online != eval(last_member_info['online']):
            return self.online
        else:
            return False

    def check_goes_offline(self, last_member_info: dict) -> bool:
        if 'online' in last_member_info and self.online != eval(last_member_info['online']):
            return  not self.online
        else:
            return False

    def check_starts_new_game(self, last_member_info: dict) -> bool:
        if self.current_game:
            if 'game_id' not in last_member_info:
                return True
            elif last_member_info['game_id'] != self.current_game.id:
                return True
            else:
                return False
        else:
            return False

    def check_ends_game(self, last_member_info: dict) -> bool:
        if 'game_id' in last_member_info and last_member_info['game_id'] != '':
            if not self.current_game:
                if self.playing:
                    logger.warn(f"{self.id} is playing but has no current game. Considering {last_member_info['game_id']} as its current game.")
                    return False
                else:
                    logger.debug(f"{self.id} end game: there is no current game")
                    return True
            elif last_member_info['game_id'] != self.current_game.id:
                logger.debug(f"{self.id} end game: cache game id {last_member_info['game_id']} / current game {self.current_game.id}")
                return True
            else:
                return False
        else:
            return False
    
    def __str__(self):
        return f"{self.id} - online/playing: {self.online}/{self.playing}"

class LichessPlayer(LichessUser):
    elo: int = 0
    elo_diff: int = None
    updated_elo: int = None

    def __init__(self, raw_data, elo) -> None:
        self.elo = elo
        self.updated_elo = elo
        super().__init__(raw_data)
    
    def update_elo(self, elo_diff:int) -> None:
        self.elo_diff = elo_diff
        self.updated_elo = self.elo + elo_diff

class LichessGamePerf:

    def __init__(self, game_name, raw_data) -> None:
        self.game_name = game_name
        self.rating = raw_data['rating'] if 'rating' in raw_data else raw_data['score']
        self.rating_confirmed = not raw_data['prov'] if 'prov' in raw_data else True
        self.progression = raw_data['prog'] if 'prog' in raw_data else 0
        self.plays = raw_data['games'] if 'games' in raw_data else raw_data['runs']

    def __str__(self) -> str:
        return f"{self.game_name} : {self.rating}{'?' if not self.rating_confirmed else ''} - prog {self.progression}"
