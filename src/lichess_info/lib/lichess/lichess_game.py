
import logging,json
from typing import List
logger = logging.getLogger(__name__)
from lichess_info.lib.lichess.lichess_user import LichessPlayer,LichessUser

class LichessGame:
    id: str
    white_player: LichessPlayer
    black_player: LichessPlayer
    winner: LichessUser
    moves: list
    rated: bool
    status: str
    variant: str
    speed: str
    finished: bool
    __raw_data : dict

    def __init__(self, raw_data: dict, player1_raw_data : dict, player2_raw_data: dict) -> None:
        self.__raw_data = raw_data
        self.id = raw_data['id']
        self.moves = raw_data['moves']
        self.finished = raw_data['status'] != 'started'
        self.perf = raw_data['perf']
        self.rated = raw_data['rated']
        self.status = raw_data['status']
        self.speed = raw_data['speed']
        self.variant = raw_data['variant']
        self.opening = raw_data['opening']['name'] if 'opening' in raw_data else None

        self.winner = raw_data['winner'] if 'winner' in raw_data else None
        try:
            if raw_data['players']['white']['user']['id'] == player1_raw_data['id']:
                self.white_player = LichessPlayer(player1_raw_data, raw_data['players']['white']['rating'])
                self.black_player = LichessPlayer(player2_raw_data, raw_data['players']['black']['rating'])
            else:
                self.white_player = LichessPlayer(player2_raw_data, raw_data['players']['white']['rating'])
                self.black_player = LichessPlayer(player1_raw_data, raw_data['players']['black']['rating'])
            if self.finished:
                if 'ratingDiff' in raw_data['players']['white']:
                    self.white_player.update_elo(raw_data['players']['white']['ratingDiff'])
                    self.black_player.update_elo(raw_data['players']['black']['ratingDiff'])
        except Exception :
            logger.exception(f"while creating game")
            logger.error(f"raw_data:\n{json.dumps(raw_data,indent=4,default=str)}")
            logger.error(f"player1:\n{json.dumps(player1_raw_data,indent=4,default=str)}")
            logger.error(f"player2:\n{json.dumps(player2_raw_data,indent=4,default=str)}")


    def has_won(self, user_id: str) -> bool:
        if self.finished:
            if self.status == 'draw':
                return None
            elif self.white_player.id == user_id:
                return self.winner == 'white'
            else:
                return self.winner == 'black'
        else:
            return None

    def get_winner(self) -> LichessPlayer:
        if self.finished:
            if self.status == 'draw':
                return None
            elif self.winner == 'white':
                return self.white_player
            elif self.winner == 'black':
                return self.black_player
        else:
            logger.warning(f"{self.id} is not finished no winner at the moment")
            return None

    def get_looser(self) -> LichessPlayer:
        if self.finished:
            if self.status == 'draw':
                return None
            elif self.winner == 'white':
                return self.black_player
            elif self.winner == 'black':
                return self.white_player
        else:
            logger.warning(f"{self.id} is not finished no winner at the moment")
            return None

    def game_url(self, user_id:str='') -> str:
        """
        returns the link to the game with color selection if player is given
        """
        if user_id == '':
            return f"https://lichess.org/{self.id}"
        elif self.white_player.id == user_id:
            return f"https://lichess.org/{self.id}/white"
        else:
            return f"https://lichess.org/{self.id}/black"
    
    def get_raw_data(self) -> dict:
        return self.__raw_data

