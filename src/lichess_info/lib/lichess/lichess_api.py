import berserk
import logging,traceback,json
from typing import List
from lichess_info.lib.lichess.lichess_game import LichessGame
from lichess_info.lib.lichess.lichess_user import LichessUser
from lichess_info.lib.lichess.lichess_team import LichessTeam
logger = logging.getLogger(__name__)

# https://berserk.readthedocs.io/en/master/
# in case of missing features: https://lichess.org/api
class LichessApi:

    def __init__(self, token:str) -> None:
        session = berserk.TokenSession(token)
        self.client = berserk.Client(session=session)

    def get_team(self, team_name:str) -> LichessTeam:

        #FIXME: throw an exception when team is not allowed
        team_members = None
        team = None
        try:
            team_members = list(self.client.users.get_by_team(team_name))
        except Exception as e:
            logger.error(f"team {team_name} is not valid or not enought rights to access it")
            logger.debug(traceback.print_exception(type(e), e, e.__traceback__))
            return team
        
        team = LichessTeam(team_name)
        team.add_members(team_members)

        if len(team.members) > 0: 
            members_realtime_data  = self.client.users.get_realtime_statuses(*team.get_members_ids(), with_game_ids=True)
            #logger.debug(json.dumps(members_realtime_data,indent=4,default=str))
            for m in members_realtime_data:
                for i,u in enumerate(team.members):
                    if m['id'] == u.id:
                        team.members[i].add_realtime_data(m)
                        if 'playingId' in m and m['playingId'] != '':
                            game = self.get_game(m['playingId'])
                            team.members[i].add_current_game(game)
                        if team.members[i].playing and not team.members[i].current_game:
                            logger.warn(f"{team.members[i].id} is playing but has no current game. real time datas:\n{m}")
                        break
        return team
    
    def get_game(self, game_id:str) -> LichessGame:
        try:
            game_data = self.client.games.export(game_id=game_id)
            white_user = self.client.users.get_by_id(game_data['players']['white']['user']['id'])[0]
            black_user = self.client.users.get_by_id(game_data['players']['black']['user']['id'])[0]
            return LichessGame(game_data,white_user,black_user)
        except Exception as e:
            logger.error(f"error this '{game_id}' doesn't returns any game")
            logger.debug(traceback.print_exception(type(e), e, e.__traceback__))
            return None
    
    def get_rt_user(self, user_id):
        try:
            return self.client.users.get_realtime_statuses(user_id, with_game_ids=True)
        except Exception as e:
            logger.error("get_rt_user() - this user '{user_id}' has no realtime data")
            logger.debug(traceback.print_exception(type(e), e, e.__traceback__))
            return None

    def get_user(self, user_id):
        try:
            return self.client.users.get_by_id(user_id)
        except Exception as e:
            logger.error("get_user() - this user '{user_id}' has been found")
            logger.debug(traceback.print_exception(type(e), e, e.__traceback__))
            return None