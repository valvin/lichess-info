from flask import Flask
from lichess_info.config import Config

app = Flask(__name__)
app.config.from_object(Config)