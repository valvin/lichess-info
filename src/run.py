
from lichess_info import app, Config
from lichess_info.web import routes
import logging

conf = Config()

# when run directly (dev mode)
if __name__ == '__main__':
    app.logger.setLevel(conf.log_level)
    app.logger.info(f"Starting instance on {conf.server_host}:{conf.server_port}{' with debug mode' if conf.server_debug else ''}")
    app.logger.info(f"Logging starting with {logging.getLevelName(conf.log_level)} level")
    app.run(host=conf.server_host, port= conf.server_port, debug=conf.server_debug)
# when run with gunicorn
else:
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(conf.log_level)
    app.logger.info(f"Logging starting with {logging.getLevelName(conf.log_level)} level")